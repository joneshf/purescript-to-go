module Go
  ( Expression(..)
  , FunctionDeclaration
  , Go
  , Identifier(..)
  , Import(..)
  , Package(..)
  , Signature(..)
  , fromModule
  , prettyPrint
  ) where

import Prelude

import CoreFn.Expr (Bind(..), Expr(..), Literal(..))
import CoreFn.Ident (Ident(..))
import CoreFn.Module (Module(..))
import CoreFn.Names (Qualified(..))
import Data.Array.NonEmpty (NonEmptyArray, fromArray)
import Data.Foldable (intercalate)
import Data.Maybe (Maybe(Nothing, Just))
import Data.Monoid (mempty)
import Data.Newtype (class Newtype)
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..))
import Data.Variant (SProxy(SProxy))
import Prettier.Printer (DOC, bracket, line, pretty, text, (<+>), (</>))
import Run (Run)
import Run.ExceptV (EXCEPTV, note, throw)

data Expression =
  StringLit String |
  Call Expression (Array Expression) |
  Null |
  Selector Expression Identifier |
  Variable Identifier

type FunctionDeclaration =
  { body :: Expression
  , name :: Identifier
  , signature :: Signature
  }

type Go =
  { functions :: NonEmptyArray FunctionDeclaration
  , imports :: Array Import
  , package :: Package
  }

newtype Identifier =
  Identifier String

derive instance newtypeIdentifier :: Newtype Identifier _

newtype Import =
  Import String

derive instance newtypeImport :: Newtype Import _

newtype Package =
  Package String

derive instance newtypePackage :: Newtype Package _

type Signature =
  { parameters :: Array Identifier
  }

docFromExpression :: Expression -> DOC
docFromExpression = case _ of
  Call expression arguments ->
    docFromExpression expression
      <> text "("
      <> intercalate (text ", ") (map docFromExpression arguments)
      <> text ")"
  Null -> mempty
  Selector expression ident ->
    docFromExpression expression <> text "." <> docFromIdentifier ident
  StringLit str -> text "\"" <> text str <> text "\""
  Variable ident -> docFromIdentifier ident

docFromFunction :: FunctionDeclaration -> DOC
docFromFunction {body, name, signature} =
  text "func"
    <+> docFromIdentifier name
    <> docFromSignature signature
    <+> bracket "{" (docFromExpression body) "}"

docFromGo :: Go -> DOC
docFromGo {functions, imports, package} =
  docFromPackage package
    </> intercalate line (map docFromImport imports)
    </> intercalate line (map docFromFunction functions)

docFromIdentifier :: Identifier -> DOC
docFromIdentifier (Identifier str) = text str

docFromImport :: Import -> DOC
docFromImport (Import name) =
  text "import"
    <+> text "\""
    <> text name
    <> text "\""
    <> text ";"
    <> line

docFromPackage :: Package -> DOC
docFromPackage (Package name) =
  text "package" <+> text name <> line

docFromSignature :: Signature -> DOC
docFromSignature {parameters} =
  case parameters of
    [] -> text "()"
    _ ->
      text "("
        <> intercalate (text ", ") (map docFromIdentifier parameters)
        <> text ")"

expressionFromExpr :: forall a. Expr a -> Expression
expressionFromExpr = case _ of
  Accessor _ ident expression ->
    Selector (expressionFromExpr expression) (Identifier ident)
  App _ f x -> Call (expressionFromExpr f) [expressionFromExpr x]
  Literal _ (StringLiteral str) -> StringLit str
  Var _ ident -> Variable (identifierFromIdent $ fromQualified ident)
  _ -> Null

fromModule ::
  forall e r.
  Package ->
  Module Unit ->
  Run (except :: EXCEPTV (generatedImport :: Import, noFunctions :: Unit | e) | r) Go
fromModule package (Module {moduleDecls, moduleForeign}) = do
  imports <- traverse importFromIdent moduleForeign
  functions <- functionsFromBinds moduleDecls
  pure {functions, imports, package}

fromQualified :: forall a. Qualified a -> a
fromQualified (Qualified _ x) = x

functionFromBind :: forall a. Bind a -> Array FunctionDeclaration
functionFromBind (Bind bs) = map go bs
  where
  go (Tuple (Tuple _ ident) expr) = {body, name, signature}
    where
    body = expressionFromExpr expr
    name = identifierFromIdent ident
    signature = signatureFromExpr expr

functionsFromBinds ::
  forall a e r.
  Array (Bind a) ->
  Run (except :: EXCEPTV (noFunctions :: Unit | e) | r) (NonEmptyArray FunctionDeclaration)
functionsFromBinds bs =
  note
    (SProxy :: SProxy "noFunctions")
    unit
    (fromArray (functionFromBind =<< bs))

identifierFromIdent :: Ident -> Identifier
identifierFromIdent = case _ of
  GenIdent (Just str) n -> Identifier (str <> show n)
  GenIdent Nothing n -> Identifier ("u" <> show n)
  Ident str -> Identifier str

importFromIdent ::
  forall e r.
  Ident ->
  Run (except :: EXCEPTV (generatedImport :: Import | e) | r) Import
importFromIdent = case _ of
  GenIdent (Just str) n ->
    throw (SProxy :: SProxy "generatedImport") $ Import (str <> show n)
  GenIdent Nothing n ->
    throw (SProxy :: SProxy "generatedImport") $ Import ("u" <> show n)
  Ident str -> pure (Import str)

prettyPrint :: Go -> String
prettyPrint go = pretty 4 (docFromGo go)

signatureFromExpr :: forall a. Expr a -> Signature
signatureFromExpr = case _ of
  Abs _ ident expr -> {parameters: [identifierFromIdent ident]}
  _ -> {parameters: []}
