module Run.ExceptV where

import Data.Maybe (Maybe)
import Data.Symbol (class IsSymbol, SProxy)
import Data.Variant (class VariantMatchCases, Variant, inj, match)
import Run (Run)
import Run.Except (EXCEPT)
import Run.Except as Run.Except
import Type.Row (class RowToList)

type EXCEPTV r = EXCEPT (Variant r)

catch ::
  forall a e i list r row.
  RowToList row list =>
  Union i () e =>
  VariantMatchCases list i (Run r a) =>
  Record row ->
  Run (except :: EXCEPTV e | r) a ->
  Run r a
catch handlers = Run.Except.catch (match handlers)

note ::
  forall a b don't_care e r s.
  IsSymbol s =>
  RowCons s b don't_care e =>
  SProxy s ->
  b ->
  Maybe a ->
  Run (except :: EXCEPTV e | r) a
note s x = Run.Except.note (inj s x)

throw ::
  forall a b don't_care e r s.
  IsSymbol s =>
  RowCons s b don't_care e =>
  SProxy s ->
  b ->
  Run (except :: EXCEPTV e | r) a
throw s x = Run.Except.throw (inj s x)
