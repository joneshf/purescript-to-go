module Test.Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE)

main :: forall e. Eff (console :: CONSOLE | e) Unit
main = do
  fmt."Println" "You should add some tests."

foreign import fmt ::
  forall e.
  { "Println" :: String -> Eff (console :: CONSOLE | e) Unit
  }
